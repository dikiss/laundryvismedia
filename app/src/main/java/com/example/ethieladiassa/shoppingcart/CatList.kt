package com.example.ethieladiassa.shoppingcart

import com.google.gson.annotations.SerializedName

data class CatList(
    @SerializedName("order_no")
    var order_no: String? = null,

    @SerializedName("id")
    var id: Int? = null,

    @SerializedName("cat_title")
    var cat_title: String? = null,


    @SerializedName("items_list")
    var itemsList: List<ItemsList> = arrayListOf()
)